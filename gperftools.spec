Name:	 gperftools
Version: 2.13
Release: 1
Summary: high-performance malloc and performance analysis tools

License: BSD-3-Clause
URL:	 https://github.com/gperftools/gperftools
Source0: https://github.com/gperftools/gperftools/releases/download/%{name}-%{version}/%{name}-%{version}.tar.gz

patch1: gperftools-generic-dynamic-tls.patch

Patch9000: skip-arm-in-stacktrace_unittest.patch
#Patch9001: skip-heapchecker-in-arm-arch.patch

BuildRequires: autoconf automake gcc-c++
BuildRequires: libtool libunwind-devel perl-generators
Requires: %{name}-libs = %{version}-%{release}

ExcludeArch:	s390

%description
gperftools is a collection of a high-performance multi-threaded \
malloc() implementation, plus some pretty nifty performance analysis \
tools.

%package libs
Summary: Libraries for CPU profiler and tcmalloc
Provides: google-perftools-libs = %{version}-%{release}
Obsoletes: google-perftools-libs < 2.0

%description libs
This package contains libraries with CPU or heap profiling and thread-caching
malloc().

%package devel
Summary:  Development header files
Requires: pkg-config
Requires: %{name}-libs = %{version}-%{release}
Provides: google-perftools-devel = %{version}-%{release}
Provides: pkg-config(gperftools-devel) = %{version}-%{release}
Obsoletes: google-perftools-devel < 2.0

%description devel
This package contains Development header files with CPU or heap profiling and
thread-caching malloc() which provides by google and gperftools.

%package -n pprof
Summary:   Analyzes CPU and heap profiles tools
Requires:  gv graphviz perl
Requires:  %{name}-devel = %{version}-%{release}
BuildArch: noarch
Provides:  pprof = %{version}-%{release}
Provides:  google-perftools = %{version}-%{release}
Obsoletes: google-perftools < 2.0

%description -n pprof
pprof is an facility that analyzes CPU and heap profiles.

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -vif

CFLAGS=`echo $RPM_OPT_FLAGS -fno-strict-aliasing -Wno-unused-local-typedefs -DTCMALLOC_LARGE_PAGES | sed -e 's|-fexceptions||g'`
CXXFLAGS=`echo $RPM_OPT_FLAGS -fno-strict-aliasing -Wno-unused-local-typedefs -DTCMALLOC_LARGE_PAGES | sed -e 's|-fexceptions||g'`

%configure \
%ifarch s390x aarch64
	--disable-general-dynamic-tls \
%endif
	--disable-dynamic-sized-delete-support \

%disable_rpath

make

%check
LD_LIBRARY_PATH=./.libs make check

%install
%make_install

%ldconfig_scriptlets libs

%files
#nothing to do

%files libs
%{_libdir}/libprofiler.so.*
%{_libdir}/libtcmalloc*.so.*

%files devel
%{_includedir}/google/*.h
%{_includedir}/gperftools/*.h
%{_libdir}/libprofiler.so
%{_libdir}/libtcmalloc*.so
%{_libdir}/pkgconfig/*.pc
%{_docdir}/%{name}/*
%{_libdir}/libprofiler.*a
%{_libdir}/libtcmalloc*.*a

%files -n pprof
%{_bindir}/*
%{_mandir}/man1/*.1.gz

%changelog
* Fri Dec 15 2023 Qingqing Li <liqingqing3@huawei.com> - 2.13-1
- upgrade to 2.13

* Wed Jul 12 2023 doupengda <doupengda@loongson.cn> - 2.10-3
- Resolving loongarch64 build error

* Wed Aug 03 2022 liukuo <liukuo@kylinos.cn> - 2.10-2
- License compliance rectification

* Tue Jul 19 2022 Qingqing Li <liqingqing3@huawei.com> - 2.10-1
- upgrade to 2.10

* Mon Feb 28 2022 liusirui <liusirui@huawei.com> - 2.9.1-3
- remove the dependency of the main package and pprof

* Fri Dec 3 2021 zhouwenpei <zhouwenpei1@huawei.com> - 2.9.1-2
- rebuild package

* Mon Nov 29 2021 zhangyiru <zhangyiru3@huawei.com> - 2.9.1-1
- update to 2.9.1

* Tue Nov 9 2021 zhangyiru <zhangyiru3@huawei.com> - 2.8.1-4
- avoid exceed int range when use heapchecker

* Wed Oct 20 2021 zhangyiru <zhangyiru3@huawei.com> - 2.8.1-3
- enable make check && skip four arm testcases. 
  the reason is that arm do not have fully functional heap checker and 
  the calling of unw_step in arm stacktrace_unittest is incorrect, but the function is not affected

* Thu Jul 22 2021 zhangyiru <zhangyiru3@huawei.com> - 2.8.1-2
- remove invalid gdb build dependency

* Sat Jan 30 2021 xinghe <xinghe1@huawei.com> - 2.8.1-1
- update to 2.8.1

* Tue Sep 22 2020 liuzixian <liuzixian4@huawei.com> - 2.8-2
- Type: bufgix
- Reason: add patch gperftools-generic-dynamic-tls.patch to avoid issue #I1VAEU which is probably caused by compiler bugs.

* Thu Jul 23 2020 jinzhimin <jinzhimin2@huawei.com> - 2.8-1
- update to 2.8

* Thu Mar 19 2020 yuxiangyang <yuxiangyang4@huawei.com> - 2.7-7
- fix build src.rpm error

* Fri Jan 10 2020 wuxu_wu <wuxu.wu@huawei.com> - 2.7-6
- delete useless patch

* Thu Aug 29 2019 luochunsheng <luochunsheng@huawei.com> - 2.7-5
- fix spelling errors

* Mon Jun 17 2019 Zhipeng Xie <xiezhipeng1@huawei.com> - 2.7-4
- Type:bugfix
- ID:NA
- SUG: NA
- DESC: issue-1122: fix bus error on aarch64

* Mon Apr 15 2019 lvying <lvying6@huawei.com> - 2.7-3
- Type:enhancement
- ID:NA
- SUG: NA
- DESC: peripheral package quality reinforcement: backport bugfix patches from community

* Tue Mar 5  2019 openEuler Buildteam <buildteam@openeuler.org> - 2.7-2
- Package init
